package click.dummer.mycorenews;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class UpdateService extends Service {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        buildUpdate();

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("mm");
        String minute = ft.format(dNow);
        if (minute.equals("05") || minute.equals("20") || minute.equals("40")) {
            new RetrieveTask().execute("http://dummer.click/_p/wdrWetter/?text=true");
        }
        return START_STICKY;
    }

    private void buildUpdate() {
        RemoteViews views = new RemoteViews(getPackageName(), R.layout.main);

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("HH:mm:ss");
        String timeStr = ft.format(dNow);
        ft = new SimpleDateFormat("cc, dd. MMMM");
        String timeStr2 = ft.format(dNow);

        views.setTextViewText(R.id.textViewTime, timeStr);
        views.setTextViewText(R.id.textViewDate, timeStr2);

        // Push update for this widget to the home screen
        ComponentName thisWidget = new ComponentName(this, MainActivity.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(this);
        manager.updateAppWidget(thisWidget, views);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class RetrieveTask extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {
            try {
                String url = urls[0];
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(url);
                HttpResponse response = null;
                try {
                    response = httpclient.execute(httpget);
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
                HttpEntity entity = response.getEntity();
                InputStream is = null;
                try {
                    is = entity.getContent();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                StringBuilder sb = null;
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8)) {
                    sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null)
                        sb.append(line + "\n");
                } catch (Exception e) {
                    return null;
                }

                String resString = sb.toString(); // Result is here

                try {
                    is.close(); // Close the stream
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
                return resString;
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(String data) {
            RemoteViews views = new RemoteViews(getPackageName(), R.layout.main);
            if (data != null) views.setTextViewText(R.id.textViewInfo, data);

            // Push update for this widget to the home screen
            ComponentName thisWidget = new ComponentName(UpdateService.this, MainActivity.class);
            AppWidgetManager manager = AppWidgetManager.getInstance(UpdateService.this);
            manager.updateAppWidget(thisWidget, views);
        }
    }
}
