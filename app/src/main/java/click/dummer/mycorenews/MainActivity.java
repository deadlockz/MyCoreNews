package click.dummer.mycorenews;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppWidgetProvider {
    public static final int SHORT_UPDATE = 1000;
    public static final int LONG_UPDATE  = 1 * 60 * 1000;

    private PendingIntent service = null;
    public Timer timer = null;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        if (timer == null) timer = new Timer();
        timer.scheduleAtFixedRate(new SecondTimer(context, appWidgetManager), 1, SHORT_UPDATE);


        final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final Calendar TIME = Calendar.getInstance();
        TIME.set(Calendar.MINUTE, 0);
        TIME.set(Calendar.SECOND, 0);
        TIME.set(Calendar.MILLISECOND, 0);
        final Intent i = new Intent(context, UpdateService.class);

        if (service == null) {
            service = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
        }

        m.setRepeating(AlarmManager.RTC, TIME.getTime().getTime(), LONG_UPDATE, service);
    }

    @Override
    public void onDisabled(Context context) {
        final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        m.cancel(service);
    }


    private class SecondTimer extends TimerTask {
        RemoteViews remoteViews;
        ComponentName thisWidget;
        AppWidgetManager appWidgetManager;

        public SecondTimer(Context context, AppWidgetManager appWiManager) {
            appWidgetManager = appWiManager;
            remoteViews = new RemoteViews(context.getPackageName(), R.layout.main);
            thisWidget = new ComponentName(context, MainActivity.class);
        }

        @Override
        public void run() {
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("HH:mm:ss");
            String timeStr = ft.format(dNow);
            ft = new SimpleDateFormat("cc, dd. MMMM");
            String timeStr2 = ft.format(dNow);
            remoteViews.setTextViewText(R.id.textViewTime, timeStr);
            remoteViews.setTextViewText(R.id.textViewDate, timeStr2);

            appWidgetManager.updateAppWidget(thisWidget, remoteViews);
        }
    }
}